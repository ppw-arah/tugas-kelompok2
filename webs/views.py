from django.shortcuts import render,redirect
from .models import WebsitesSci,WebsitesSoc
from . import forms

# Create your views here.


## Social Webs
def socWeb(request):
    title = "SocWeb"
    websites = WebsitesSoc.objects.all()
    return render(request,'socialWebs.html', {"title": title,"websites":websites})

def upvoteSocWeb(request,id):
    socWeb = WebsitesSoc.objects.get(id=id)
    socWeb.score+=1
    socWeb.save()
    return redirect("webs:socWeb")

def downvoteSocWeb(request,id):
    socWeb = WebsitesSoc.objects.get(id=id)
    socWeb.score-=1
    socWeb.save()
    return redirect("webs:socWeb")

def socWebRec(request):
    title = "SocWebRec"
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
        if request.method == 'POST':
            form = forms.AddWebSoc(request.POST)
            if form.is_valid():
                form.save()
                form = forms.AddWebSoc()
                return redirect("webs:socWeb")

        else:
            form = forms.AddWebSoc()
        return render(request, 'socWebsRec.html', {"title" :title,"form" : form})
    
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
        return render(request, 'loginfirst.html',{"inouturl":inouturl,"inoutword":inoutword,"greet":greet})


## Science Webs
def sciWeb(request):
    title = "SciWeb"
    websites = WebsitesSci.objects.all()
    return render(request,'scienceWebs.html', {"title": title,"websites":websites})


def upvoteSciWeb(request,id):
    sciWeb = WebsitesSci.objects.get(id=id)
    sciWeb.score+=1
    sciWeb.save()
    return redirect("webs:sciWeb")

def downvoteSciWeb(request,id):
    sciWeb = WebsitesSci.objects.get(id=id)
    sciWeb.score-=1
    sciWeb.save()
    return redirect("webs:sciWeb")

def sciWebRec(request):
    title = "SciWebRec"
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
        if request.method == 'POST':
            form = forms.AddWebSci(request.POST)
            if form.is_valid():
                form.save()
                form = forms.AddWebSci()
                return redirect("webs:sciWeb")

        else:
            form = forms.AddWebSci()
        return render(request, 'sciWebsRec.html', {"title" :title,"form" : form})

    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
        return render(request, 'loginfirst.html',{"inouturl":inouturl,"inoutword":inoutword,"greet":greet})
