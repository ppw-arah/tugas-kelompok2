from django.shortcuts import render

# Create your views here.
def dev(request):
    title = "Devs"
    return render(request,'dev.html', {"title": title})
