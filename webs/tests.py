from django.test import TestCase,Client
from .models import WebsitesSci,WebsitesSoc
from .views import *
from .urls import *

# Create your tests here.
class SocialWebsUnitTest(TestCase):
    def test_SocWeb_exists(self):
        WebsitesSoc.objects.create(title="A",theurl="X")
        howmany = WebsitesSoc.objects.all().count()
        self.assertEqual(howmany,1)


class ScienceWebsUnitTest(TestCase):
    def test_SciWeb_exists(self):
        WebsitesSci.objects.create(title="A",theurl="X")
        howmany = WebsitesSci.objects.all().count()
        self.assertEqual(howmany,1)


class TemplateAcces(TestCase):
    def test_scienceWebs_accessed(self):
        satu = Client().get('/webs/science/')
        self.assertTemplateUsed(satu,'scienceWebs.html')
    
    def test_socialWebs_accessed(self):
        response = Client().get('/webs/social/')
        self.assertTemplateUsed(response,'socialWebs.html')
    
    def test_sciWebsRec_accessed(self):
        response = Client().get('/webs/science/recommendation/')
        self.assertTemplateUsed(response,'loginfirst.html')

    def test_socWebsRec_accessed(self):
        response = Client().get('/webs/social/recommendation/')
        self.assertTemplateUsed(response,'loginfirst.html')
    
    def test_social_url_is_exist(self):
        response = Client().get('/webs/social/')
        self.assertEqual(response.status_code, 200)
    
    def test_social_up_exist(self):
        SciBook = WebsitesSci.objects.create(title="A",theurl="B",score=0)
        SocBook = WebsitesSoc.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/webs/sociWeb-upvote/1/')
        self.assertEqual(response.status_code, 302)
    
    def test_social_down_exist(self):
        SciBook = WebsitesSci.objects.create(title="A",theurl="B",score=0)
        SocBook = WebsitesSoc.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/webs/socWeb-downvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_up_exist(self):
        SciBook = WebsitesSci.objects.create(title="A",theurl="B",score=0)
        SocBook = WebsitesSoc.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/webs/sciWeb-upvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_down_exist(self):
        SciBook = WebsitesSci.objects.create(title="A",theurl="B",score=0)
        SocBook = WebsitesSoc.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/webs/sciWeb-downvote/1/')
        self.assertEqual(response.status_code, 302)