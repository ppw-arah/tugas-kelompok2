from django import forms
from . import models

class AddWebSci(forms.ModelForm):
    class Meta:
        model = models.WebsitesSci
        fields = ['title','theurl']

class AddWebSoc(forms.ModelForm):
    class Meta:
        model = models.WebsitesSoc
        fields = ['title','theurl']