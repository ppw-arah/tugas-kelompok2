from django.urls import path
from . import views

app_name = 'gameApp'

urlpatterns = [
    path('', views.gameSearch, name='gameSearch'),
]