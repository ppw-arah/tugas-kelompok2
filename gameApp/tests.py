from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .views import *
from .urls import *
from .apps import *

# Create your tests here.
class GameAppUnitTest(TestCase): 
    def test_gameApp_url_is_exist(self):
        response = Client().get('/gameApp')
        self.assertEqual(response.status_code, 301)

    def test_gameApp_url_is_not_exist(self):
        response = Client().get('/searchGame')
        self.assertEqual(response.status_code, 404)