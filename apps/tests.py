from django.test import TestCase,Client
from .models import Applications,Applications2
from .views import *
from .urls import *

# Create your tests here.
class SocialAppsUnitTest(TestCase):
    def test_SocApp_exists(self):
        Applications2.objects.create(title="A",theurl="X")
        howmany = Applications2.objects.all().count()
        self.assertEqual(howmany,1)


class ScienceAppsUnitTest(TestCase):
    def test_SciApp_exists(self):
        Applications.objects.create(title="A",theurl="X")
        howmany = Applications.objects.all().count()
        self.assertEqual(howmany,1)


class TemplateAcces(TestCase):

    def test_scienceApps_accessed(self):
        satu = Client().get('/apps/science/')
        self.assertTemplateUsed(satu,'scienceApps.html')
    
    def test_socialApps_accessed(self):
        response = Client().get('/apps/social/')
        self.assertTemplateUsed(response,'socialApps.html')
    
    def test_sciAppsRec_accessed(self):
        response = Client().get('/apps/science/recommendation/')
        self.assertTemplateUsed(response,'loginfirst.html')

    def test_socAppsRec_accessed(self):
        response = Client().get('/apps/social/recommendation/')
        self.assertTemplateUsed(response,'loginfirst.html')
    
    def test_social_url_is_exist(self):
        response = Client().get('/apps/social/')
        self.assertEqual(response.status_code, 200)
    
    def test_social_up_exist(self):
        SciApp = Applications.objects.create(title="A",theurl="B",score=0)
        SocApp = Applications2.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/apps/sociApp-upvote/1/')
        self.assertEqual(response.status_code, 302)
    
    def test_social_down_exist(self):
        SciApp = Applications.objects.create(title="A",theurl="B",score=0)
        SocApp = Applications2.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/apps/socApp-downvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_up_exist(self):
        SciApp = Applications.objects.create(title="A",theurl="B",score=0)
        SocApp = Applications2.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/apps/sciApp-upvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_down_exist(self):
        SciApp = Applications.objects.create(title="A",theurl="B",score=0)
        SocApp = Applications2.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/apps/sciApp-downvote/1/')
        self.assertEqual(response.status_code, 302)

    