from django import forms
from . import models

class AddBookSci(forms.ModelForm):
    class Meta:
        model = models.SciBooks
        fields = ['title','theurl']

class AddBookSoc(forms.ModelForm):
    class Meta:
        model = models.SocBooks
        fields = ['title','theurl']