from django.test import TestCase,Client
from .views import *
from .urls import *

# Create your tests here.


class MusicSearch(TestCase):
    def test_musicSearchTest_accessed(self):
        response = Client().get('/musicSearch/')
        self.assertTemplateUsed(response,'musicSearch.html')
    
    def test_musicSearch_url_is_exist(self):
        response = Client().get('/musicSearch/')
        self.assertEqual(response.status_code, 200)
