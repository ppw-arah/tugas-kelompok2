$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url : 'https://www.googleapis.com/books/v1/volumes?q=studies',
        success: function(response) {
            $('tbody').empty();
            for(let i=0; i < response.items.length; i++) {
                var row = document.createElement('tr');
                $(row).append('<td class="title text-center">' + response.items[i].volumeInfo.title + '</td>');
                try {
                    $(row).append('<td class="desc">' + response.items[i].volumeInfo.description + '</td>');
                }
                catch {
                    $(row).append('<td class="desc">No description available</td>');
                }
                $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>')
                try {
                    $(row).append('<td class="imgg"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                catch(e) {
                    $(row).append('<td class="imgg">No image available</td>')
                }
                $('tbody').append(row);
            }
        }
    })


    $('#button').click(function() {
        let key  = $('#search').val();
        $.ajax({
            method: 'GET',
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('tbody').empty();
                for(let i=0; i < response.items.length; i++) {
                    var row = document.createElement('tr');
                    $(row).append('<td class="title text-center">' + response.items[i].volumeInfo.title + '</td>');
                    try {
                        $(row).append('<td class="desc">' + response.items[i].volumeInfo.description + '</td>');
                    }
                    catch {
                        $(row).append('<td class="desc">No description available</td>');
                    }
                    $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>')
                    try {
                        $(row).append('<td class="imgg"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                    }
                    catch(e) {
                        $(row).append('<td class="imgg">No image available</td>')
                    }
                    $('tbody').append(row);
                }
            }
        })
    })
})
