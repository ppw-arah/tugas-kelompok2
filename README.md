# PPW Kelompok ARAH
## Members
1. Ali Asadillah
2. Hanrichie
3. Muzaki Azami K
4. Riri Edwina 

## Link
https://arah2.herokuapp.com/

## About our application
Our apps helps college students finding everything they need academically, so they can focus on the thing that matters, using them.
The apps will give college students, depending on their major, a list of apps, books, and websites that they need. 
Users can submit their own reccomendation of apps/webs/books. Every recommendations can be rated by the users, to keep the quality
of the recommendations in check.

## Implemented Features
- Recommendation submission for several types of categories (Webs,Books,Applications)
- Developers' page that links to our respective online portfolio
- Search engine that guides people to what they want (Music,Game,Book)

## Code Coverage
|Name                                         |Stmts |  Miss | Cover |   Missing |
|---------------------------------------------|------|-------|------ |----------
|accounts\__init__.py                         |    0 |     0 |  100% |
|accounts\migrations\__init__.py              |    0 |     0 |  100% |
|accounts\tests.py                            |   10 |     0 |  100% |
|accounts\urls.py                             |    4 |     0 |  100% |
|accounts\views.py                            |   32 |    14 |   56% |   9-13, 21-22, 27-29, 36-39
|apps\__init__.py                             |    0 |     0 |  100% |
|apps\admin.py                                |    4 |     0 |  100% |
|apps\forms.py                                |   10 |     0 |  100% |
|apps\migrations\0001_initial.py              |    5 |     0 |  100% |
|apps\migrations\0002_auto_20191013_1204.py   |    4 |     0 |  100% |
|apps\migrations\0003_applications_score.py   |    4 |     0 |  100% |
|apps\migrations\0004_applications2.py        |    4 |     0 |  100% |
|apps\migrations\__init__.py                  |    0 |     0 |  100% |
|apps\models.py                               |   15 |     3 |   80% |   10, 13, 21
|apps\tests.py                                |   50 |     0 |  100% |
|apps\urls.py                                 |    4 |     0 |  100% |
|apps\views.py                                |   84 |    28 |   67% |   15-17, 41-52, 64-66, 91-103
|arah\__init__.py                             |    0 |     0 |  100% |
|arah\settings.py                             |   25 |     2 |   92% |   91-92
|arah\urls.py                                 |    3 |     0 |  100% |
|books\__init__.py                            |    0 |     0 |  100% |
|books\admin.py                               |    4 |     0 |  100% |
|books\forms.py                               |   10 |     0 |  100% |
|books\migrations\0001_initial.py             |    5 |     0 |  100% |
|books\migrations\__init__.py                 |    0 |     0 |  100% |
|books\models.py                              |   15 |     3 |   80% |   10, 13, 21
|books\tests.py                               |   50 |     0 |  100% |
|books\urls.py                                |    4 |     0 |  100% |
|books\views.py                               |   51 |    10 |   80% |   29-33, 61-65
|gameApp\__init__.py                          |    0 |     0 |  100% |
|gameApp\apps.py                              |    3 |     0 |  100% |
|gameApp\migrations\__init__.py               |    0 |     0 |  100% |
|gameApp\tests.py                             |   13 |     0 |  100% |
|gameApp\urls.py                              |    4 |     0 |  100% |
|gameApp\views.py                             |    3 |     1 |   67% |   5
|home\__init__.py                             |    0 |     0 |  100% |
|home\admin.py                                |    1 |     0 |  100% |
|home\migrations\__init__.py                  |    0 |     0 |  100% |
|home\models.py                               |    1 |     0 |  100% |
|home\tests.py                                |    1 |     0 |  100% |
|home\urls.py                                 |    4 |     0 |  100% |
|home\views.py                                |   11 |     9 |   18% |   7-16
|musicSearch\__init__.py                      |    0 |     0 |  100% |
|musicSearch\migrations\__init__.py           |    0 |     0 |  100% |
|musicSearch\tests.py                         |   10 |     0 |  100% |
|musicSearch\urls.py                          |    4 |     0 |  100% |
|musicSearch\views.py                         |    3 |     0 |  100% |
|searchBook\__init__.py                       |    0 |     0 |  100% |
|searchBook\admin.py                          |    1 |     0 |  100% |
|searchBook\migrations\__init__.py            |    0 |     0 |  100% |
|searchBook\models.py                         |    1 |     0 |  100% |
|searchBook\tests.py                          |   10 |     0 |  100% |
|searchBook\urls.py                           |    4 |     0 |  100% |
|searchBook\views.py                          |    4 |     0 |  100% |
|webs\__init__.py                             |    0 |     0 |  100% |
|webs\admin.py                                |    4 |     0 |  100% |
|webs\forms.py                                |   10 |     0 |  100% |
|webs\migrations\0001_initial.py              |    5 |     0 |  100% |
|webs\migrations\__init__.py                  |    0 |     0 |  100% |
|webs\models.py                               |   15 |     3 |   80% |   10, 13, 21
|webs\tests.py                                |   50 |     0 |  100% |
|webs\urls.py                                 |    4 |     0 |  100% |
|webs\views.py                                |   51 |    10 |   80% |   29-33, 61-65
|TOTAL                                         | 614 |    83 |   86% |
