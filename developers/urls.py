from django.urls import path
from . import views
app_name = "developers"
urlpatterns = [
    path('developers/', views.dev, name="developers"),
]
