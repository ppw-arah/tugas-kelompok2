from django.urls import path
from . import views
app_name = "books"
urlpatterns = [
    path('science/', views.sciBook, name="sciBook"),
    path('social/',views.socBook, name="socBook"),
    path('science/recommendation/', views.sciBookRec, name="sciBookRec"),
    path('sciBook-upvote/<id>/', views.upvoteSciBook, name = 'upvoteSciBook'),
    path('sciBook-downvote/<id>/', views.downvoteSciBook, name = 'downvoteSciBook'),
    path('social/recommendation/', views.socBookRec, name="socBookRec"),
    path('socBook-upvote/<id>/', views.upvoteSocBook, name = 'upvoteSocBook'),
    path('socBook-downvote/<id>/', views.downvoteSocBook, name = 'downvoteSocBook'),
]
