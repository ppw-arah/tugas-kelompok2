from django.contrib import admin
from .models import WebsitesSci,WebsitesSoc

# Register your models here.
admin.site.register(WebsitesSci)
admin.site.register(WebsitesSoc)