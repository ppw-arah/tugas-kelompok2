from django.shortcuts import render,redirect
#from .models import Agenda
#from . import forms

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
    title = "HOME"
    return render(request,'Home.html', {"title": title,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})