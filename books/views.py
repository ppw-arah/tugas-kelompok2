from django.shortcuts import render,redirect
from .models import SciBooks, SocBooks    ##CHANGE
from . import forms

# Create your views here.


## Social Book
def socBook(request):
    title = "SocBook"
    books = SocBooks.objects.all()
    return render(request,'socialBooks.html', {"title": title,"books":books})

def upvoteSocBook(request,id):
    socBook = SocBooks.objects.get(id=id)
    socBook.score+=1
    socBook.save()
    return redirect("books:socBook")

def downvoteSocBook(request,id):
    socBook = SocBooks.objects.get(id=id)
    socBook.score-=1
    socBook.save()
    return redirect("books:socBook")

def socBookRec(request):
    title = "SocBookRec"
    if request.method == 'POST':
        form = forms.AddBookSoc(request.POST)
        if form.is_valid():
            form.save()
            form = forms.AddBookSoc()
            return redirect("books:socBook")

    else:
        form = forms.AddBookSoc()
    return render(request, 'socBooksRec.html', {"title" :title,"form" : form})

## Science Books
def sciBook(request):
    title = "SciBook"
    books = SciBooks.objects.all()
    return render(request,'scienceBooks.html', {"title": title,"books":books})


def upvoteSciBook(request,id):
    sciBook = SciBooks.objects.get(id=id)
    sciBook.score+=1
    sciBook.save()
    return redirect("books:sciBook")

def downvoteSciBook(request,id):
    sciBook = SciBooks.objects.get(id=id)
    sciBook.score-=1
    sciBook.save()
    return redirect("books:sciBook")

def sciBookRec(request):
    title = "SciBookRec"
    if request.method == 'POST':
        form = forms.AddBookSci(request.POST)
        if form.is_valid():
            form.save()
            form = forms.AddBookSci()
            return redirect("books:sciBook")

    else:
        form = forms.AddBookSci()
    return render(request, 'sciBooksRec.html', {"title" :title,"form" : form})
