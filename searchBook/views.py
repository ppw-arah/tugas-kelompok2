from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
def searchBook(request):
    return render(request, "searchBook.html")
