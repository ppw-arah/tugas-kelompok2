from django.test import TestCase,Client
from .views import *
from .urls import *

# Create your views here.

class Accounts(TestCase):
    def test_signup(self):
        response = Client().get('/accounts/signup/')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
