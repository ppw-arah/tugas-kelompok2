from django.urls import path
from . import views

app_name = "webs"

urlpatterns = [
    path('science/', views.sciWeb, name="sciWeb"),
    path('social/',views.socWeb, name="socWeb"),
    path('science/recommendation/', views.sciWebRec, name="sciWebRec"),
    path('sciWeb-upvote/<id>/', views.upvoteSciWeb, name = 'upvoteSciWeb'),
    path('sciWeb-downvote/<id>/', views.downvoteSciWeb, name = 'downvoteSciWeb'),
    path('social/recommendation/', views.socWebRec, name="socWebRec"),
    path('sociWeb-upvote/<id>/', views.upvoteSocWeb, name = 'upvoteSocWeb'),
    path('socWeb-downvote/<id>/', views.downvoteSocWeb, name = 'downvoteSocWeb'),
]
