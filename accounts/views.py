from django.shortcuts import render
from django.shortcuts import render,redirect,HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout

# Create your views here.
def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('/')
    else:
        form = AuthenticationForm()
    return render(request, 'loginform.html',{'form':form})



def logout_view(request):
    logout(request)
    return redirect('/')


def signup_view(request):
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
    title = "SIGNUP"
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/accounts/login')
    else:
        form = UserCreationForm()
    return render(request, 'signupform.html',{'form':form,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})
