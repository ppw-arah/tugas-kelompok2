from django.test import TestCase,Client
from .models import SciBooks, SocBooks
from .views import *
from .urls import *

# Create your tests here.
class SocialBooksUnitTest(TestCase):
    def test_SocBook_exists(self):
        SocBooks.objects.create(title="A",theurl="X")
        howmany = SocBooks.objects.all().count()
        self.assertEqual(howmany,1)


class ScienceBooksUnitTest(TestCase):
    def test_SciBook_exists(self):
        SciBooks.objects.create(title="A",theurl="X")
        howmany = SciBooks.objects.all().count()
        self.assertEqual(howmany,1)


class TemplateAcces(TestCase):
    def test_scienceBooks_accessed(self):
        satu = Client().get('/books/science/')
        self.assertTemplateUsed(satu,'scienceBooks.html')
    
    def test_socialBooks_accessed(self):
        response = Client().get('/books/social/')
        self.assertTemplateUsed(response,'socialBooks.html')
    
    def test_sciBooksRec_accessed(self):
        response = Client().get('/books/science/recommendation/')
        self.assertTemplateUsed(response,'sciBooksRec.html')

    def test_socBooksRec_accessed(self):
        response = Client().get('/books/social/recommendation/')
        self.assertTemplateUsed(response,'socBooksRec.html')
    
    def test_social_url_is_exist(self):
        response = Client().get('/books/social/')
        self.assertEqual(response.status_code, 200)

    def test_social_up_exist(self):
        SciBook = SciBooks.objects.create(title="A",theurl="B",score=0)
        SocBook = SocBooks.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/books/socBook-upvote/1/')
        self.assertEqual(response.status_code, 302)
    
    def test_social_down_exist(self):
        SciBook = SciBooks.objects.create(title="A",theurl="B",score=0)
        SocBook = SocBooks.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/books/socBook-downvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_up_exist(self):
        SciBook = SciBooks.objects.create(title="A",theurl="B",score=0)
        SocBook = SocBooks.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/books/sciBook-upvote/1/')
        self.assertEqual(response.status_code, 302)

    def test_science_down_exist(self):
        SciBook = SciBooks.objects.create(title="A",theurl="B",score=0)
        SocBook = SocBooks.objects.create(title="A",theurl="B",score=0)
        response = Client().get('/books/sciBook-downvote/1/')
        self.assertEqual(response.status_code, 302)