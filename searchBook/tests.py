from django.test import TestCase,Client
from .views import *
from .urls import *

# Create your tests here.
class SearchBookTest(TestCase):
    def test_searchBooksTest_accessed(self):
        response = Client().get('/searchBook/')
        self.assertTemplateUsed(response,'searchBook.html')
    
    def test_searchBook_url_is_exist(self):
        response = Client().get('/searchBook/')
        self.assertEqual(response.status_code, 200)