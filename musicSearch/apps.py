from django.apps import AppConfig


class MusicsearchConfig(AppConfig):
    name = 'musicSearch'
