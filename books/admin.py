from django.contrib import admin
from .models import SciBooks, SocBooks
# Register your models here.
admin.site.register(SciBooks)

admin.site.register(SocBooks)