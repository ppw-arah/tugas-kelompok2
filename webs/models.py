from django.db import models

# Create your models here.
class WebsitesSci(models.Model):
    title = models.CharField(max_length=100)
    theurl = models.CharField(max_length=500,default="google.com")
    score = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def upvote(self):
        self.score+=1

class WebsitesSoc(models.Model):
    title = models.CharField(max_length=100)
    theurl = models.CharField(max_length=500,default="google.com")
    score = models.IntegerField(default=0)

    def __str__(self):
        return self.title