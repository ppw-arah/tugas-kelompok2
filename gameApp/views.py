from django.shortcuts import render

# Create your views here.
def gameSearch(request):
    return render(request, 'gameSearch.html')