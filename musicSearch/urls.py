from django.urls import path
from . import views

app_name = "musicSearch"

urlpatterns = [
    path('', views.index, name = 'musicSearch'),
]
