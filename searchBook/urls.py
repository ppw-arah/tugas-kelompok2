from django.urls import path
from . import views

app_name = "searchBook"

urlpatterns = [
    path('', views.searchBook, name = 'searchBook'),
]
