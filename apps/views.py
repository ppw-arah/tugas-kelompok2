from django.shortcuts import render,redirect
from .models import Applications,Applications2    ##CHANGE
from . import forms
## ADD THESE LINES BELOW ##
from django.shortcuts import render,redirect,HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login,logout

# Create your views here.


## Social Apps
def socApp(request):
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
    title = "SocApp"
    applications = Applications2.objects.all()
    return render(request,'socialApps.html', {"title": title,"applications":applications,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})

def upvoteSocApp(request,id):   ##TEST
    socApp = Applications2.objects.get(id=id)
    socApp.score+=1
    socApp.save()
    return redirect("apps:socApp")

def downvoteSocApp(request,id): ##TEST
    socApp = Applications2.objects.get(id=id)
    socApp.score-=1
    socApp.save()
    return redirect("apps:socApp")

def socAppRec(request): ##TEST
    title = "SocAppRec"
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
        if request.method == 'POST':
            form = forms.AddAppSoc(request.POST)
            if form.is_valid():
                form.save()
                form = forms.AddAppSoc()
                return redirect("apps:socApp")
        else:
            form = forms.AddAppSoc()
        return render(request, 'socAppsRec.html', {"title" :title,"form" : form,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
        return render(request, 'loginfirst.html',{"inouturl":inouturl,"inoutword":inoutword,"greet":greet})
    
    

## Science Apps
def sciApp(request):
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
    title = "SciApp"
    applications = Applications.objects.all()
    return render(request,'scienceApps.html', {"title": title,"applications":applications,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})


def upvoteSciApp(request,id):   ##TEST
    sciApp = Applications.objects.get(id=id)
    sciApp.score+=1
    sciApp.save()
    return redirect("apps:sciApp")

def downvoteSciApp(request,id): ##TEST
    sciApp = Applications.objects.get(id=id)
    sciApp.score-=1
    sciApp.save()
    return redirect("apps:sciApp")

def sciAppRec(request):
    title = "SciAppRec"
    if request.user.is_authenticated:
        inouturl = '/accounts/logout'
        inoutword = 'LOGOUT'
        greet = "Hello, " + request.user.username + "!"
        if request.method == 'POST':
            form = forms.AddAppSci(request.POST)
            if form.is_valid():
                form.save()
                form = forms.AddAppSci()
                return redirect("apps:sciApp")

        else:
            form = forms.AddAppSci()
        return render(request, 'sciAppsRec.html', {"title" :title,"form" : form,'inouturl':inouturl,'inoutword':inoutword,"greet":greet})
    else:
        inouturl = '/accounts/login'
        inoutword = 'LOGIN'
        greet = ""
        return render(request, 'loginfirst.html',{"inouturl":inouturl,"inoutword":inoutword,"greet":greet})
    
    
