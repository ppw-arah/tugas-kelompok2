from django.urls import path
from . import views
app_name = "apps"
urlpatterns = [
    path('science/', views.sciApp, name="sciApp"),
    path('social/',views.socApp, name="socApp"),
    path('science/recommendation/', views.sciAppRec, name="sciAppRec"),
    path('sciApp-upvote/<id>/', views.upvoteSciApp, name = 'upvoteSciApp'),
    path('sciApp-downvote/<id>/', views.downvoteSciApp, name = 'downvoteSciApp'),
    path('social/recommendation/', views.socAppRec, name="socAppRec"),
    path('sociApp-upvote/<id>/', views.upvoteSocApp, name = 'upvoteSocApp'),
    path('socApp-downvote/<id>/', views.downvoteSocApp, name = 'downvoteSocApp'),
]
